<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }


    public function index()
    {
        //get data from table post
        $posts = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success'   => true,
            'message'   => 'List Data Post',
            'data'      => $posts ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(),   [
            'title' =>  'required',
            'description'   =>  'required'
        ]);

        //response error validation
        if($validator->fails()) {
            return response()->json($validator->errors(),   400);
        }

            $user = auth()->user();
        //save to database
        $post = Post::create([
            'title' =>  $request->title,
            'description'   =>  $request->description,
            'user_id' => $user->id
        ]);

        //success save to database
        if($post){

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Post Created',
                'data'      =>  $post
            ],  201);
        }

        //failed to database
        return response()->json([
            'success'   => false,
            'message'   => 'Post Failed to save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find Post by ID
        $post = Post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success'   =>  true,
            'message'   =>  'Detail Data Post',
            'data'      =>  $post ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //set validation
        $validator = Validator::make($request->all(),   [
            'title' =>  'required',
            'description'   =>  'required',
        ]);

        //response error validation
        if ($validator->fails())    {
            return response()->json($validator->errors(),   400);
        }

        //find post by ID
        $id = 'id';
        $post = Post::find($id);

        

        if($post)   {

            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success'   =>  false,
                    'message'   =>  'Data post tidak sesuai dengan user login',
                    'data'      =>  $post
                ], 403);
            }
            //update post
            $post->update([
                'title' =>  $request->title,
                'description'   =>  $request->description
            ]);

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Post Updated : ' . $post->title . ' Berhasil ditambahkan',
                'data'      =>  $post
            ],  200);

        }

        //data post not found
        return response()->json([
            'success'   =>  false,
            'message'   =>  'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post)   {

            $user = auth()->user();
            //delete post
            $post->delete();

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Post Deleted',
            ],  200);
        }

        //data post not found
        return response()->json([
            'success'   =>  false,
            'message'   =>  'Post Not Found',
        ],  404);
    }
}
