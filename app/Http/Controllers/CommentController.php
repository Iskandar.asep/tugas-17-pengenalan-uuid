<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    public function index()
    {
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success'   => true,
            'message'   => 'List Data Comment',
            'data'      => $comments ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),   [
            'content' =>  'required',
            'post_id'   =>  'required'
        ]);

        //response error validation
        if($validator->fails()) {
            return response()->json($validator->errors(),   400);
        }

        //save to database
        $comment = Comment::create([
            'content' =>  $request->content,
            'post_id'   =>  $request->post_id
        ]);

        //success save to database
        if($comment){

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Comment Created',
                'data'      =>  $comment
            ],  201);
        }

        //failed to database
        return response()->json([
            'success'   => false,
            'message'   => 'Comment Failed to save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success'   =>  true,
            'message'   =>  'Detail Data Comment',
            'data'      =>  $comment ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),   [
            'content' =>  'required',
            'post_id'   =>  'required',
        ]);

        //response error validation
        if ($validator->fails())    {
            return response()->json($validator->errors(),   400);
        }

        //find post by ID
        $comment = Comment::findOrfail($id);

        if($comment)   {

            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success'   =>  false,
                    'message'   =>  'Data post tidak sesuai dengan user login',
                    'data'      =>  $comment
                ], 403);
            }

            //update post
            $comment->update([
                'content' =>  $request->content,
                'post_id'   =>  $request->post_id
            ]);

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Post Updated',
                'data'      =>  $comment
            ],  200);

        }

        //data post not found
        return response()->json([
            'success'   =>  false,
            'message'   =>  'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrfail($id);

        if($comment)   {

            $user = auth()->user();
            //delete post
            $comment->delete();

            return response()->json([
                'success'   =>  true,
                'message'   =>  'Comment Deleted',
            ],  200);
        }

        //data post not found
        return response()->json([
            'success'   =>  false,
            'message'   =>  'Comment Not Found',
        ],  404);
    }
}
